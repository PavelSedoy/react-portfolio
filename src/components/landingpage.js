import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

class Landing extends Component {
    render() {
        return(
            <div style={{width: '100%', margin: 'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img
                            src="https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png"
                            alt="avatar"
                            className="avatar-img"
                            />

                        <div className="banner-text">
                            <h1>Full Stack Web Developer</h1>

                            <hr/>

                            <p>HTML/CSS | Bootstrap | JavaScript | React | React Native | NodeJS | Express | MangoDB</p>

                            <div className="social-links">

                                {/* LinkedIn */}
                                <a href="https://www.linkedin.com/in/%D0%BF%D0%B0%D0%B2%D0%B5%D0%BB-%D1%81%D0%B5%D0%B4%D0%BE%D0%B9-193184154/" rel="noopener noreferrer" target="_blank">
                                    <i class="fab fa-linkedin" aria-hidden="true" />
                                </a>

                                {/* BitBucket */}
                                <a href="https://bitbucket.org/PavelSedoy/" rel="noopener noreferrer" target="_blank">
                                    <i class="fab fa-bitbucket" aria-hidden="true" />
                                </a>

                                {/* FreeCodeCamp */}
                                <a href="https://learn.freecodecamp.org/" rel="noopener noreferrer" target="_blank">
                                    <i class="fab fa-free-code-camp" aria-hidden="true" />
                                </a>

                                {/* YouTube */}
                                <a href="https://www.youtube.com/channel/UCDSypjZVj4dUxhzzkyK5nLg?view_as=subscriber" rel="noopener noreferrer" target="_blank">
                                    <i class="fab fa-youtube" aria-hidden="true" />
                                </a>

                            </div>
                        </div>
                    </Cell>
                </Grid>
            </div>
        )
    }
}

export default Landing;